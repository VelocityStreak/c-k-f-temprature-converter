#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <ncurses.h>

int main(void)
{
	char temp[156];
	float ftemp;
	float c, f, k;
	printf("Enter the temprature, put units after it!( lowercase )\n");
	scanf("%s", temp);

	if (strchr(temp, 'c'))
	{
		printf("celsius\n");
		temp[strlen(temp)-1] = '\0';
		printf("string valuated:%s\n", temp);
		ftemp = atof(temp);
		printf("float valuated:%f\n", ftemp);
		c = ftemp;
		f = c * 1.8 + 32;
		k = c + 273.15;
	}
	if (strchr(temp, 'k'))
	{
		printf("kelvin\n");
		temp[strlen(temp)-1] = '\0';
		printf("string evaluated:%s\n", temp);
		ftemp = atof(temp);
		printf("float evaluated:%f\n", ftemp);
		k = ftemp;
		//f = (k - 273.15) * 9 / 5 + 32;
		f = (k - 273.15) * 9 / 5 + 32;
		c = k - 273.15;
	}
	if (strchr(temp, 'f'))
	{
		printf("fahrenheit\n");
		temp[strlen(temp)-1] = '\0';
		printf("string evaluated:%s\n", temp);
		ftemp = atof(temp);
		printf("float evaluated:%f\n", ftemp);
		f = ftemp;
		c = (f - 32) * 5 / 9;
		k = (f - 32) * 5 / 9 + 273.15;
	}

	printf("celsius\t\tkelvin\t\tfahrenheit\n");
	printf("%f\t%f\t%f\n", c, k, f);

	return 0;
}	
